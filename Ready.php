<?php
	header( "Pragma: No-Cache" );
	include( "./inc/function.php" );

	/******************************************************************************** 
	 *
	 * 다날 본인인증
	 *
	 * - 인증 요청 페이지
	 *	CP인증 및 기타 정보 전달
	 *
	 * 문의사항이 있으시면 아래의 메일주소로 문의 주시기 바랍니다.
	 * DANAL Commerce Division Technique supporting Team
	 * EMAIL : tech@danal.co.kr
	 *
	 ********************************************************************************/

	/********************************************************************************
	 *
	 * XSS 취약점 방지를 위해 
	 * 모든 페이지에서 파라미터 값에 대해 검증하는 로직을 추가할 것을 권고 드립니다.
	 * XSS 취약점이 존재할 경우 웹페이지를 열람하는 접속자의 권한으로 부적절한 스크립트가 수행될 수 있습니다.
	 * 보안 대책
	 *  - html tag를 허용하지 않아야 합니다.(html 태그 허용시 white list를 선정하여 해당 태그만 허용)
	 *  - <, >, &, " 등의 문자를 replace등의 문자 변환함수를 사용하여 치환해야 합니다.
	 * 
	 ********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>다날 본인확인</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
</head>
<?php
	/******************************************************************************** 
	 *
	 * [ 전문 요청 데이터 ] *********************************************************
	 *
	 ********************************************************************************/

	/***[ 필수 데이터 ]************************************/
	$TransR = array();

	/******************************************************
	 ** 아래의 데이터는 고정값입니다.( 변경하지 마세요 )
	 * TXTYPE	: ITEMSEND
	 * SERVICE	: UAS 
	 * AUTHTYPE	: 36
	 ******************************************************/
	$TransR["TXTYPE"] = "ITEMSEND";
	$TransR["SERVICE"] = "UAS";
	$TransR["AUTHTYPE"] = "36";

	/******************************************************
	 * CPID	: 다날에서 제공해 드린 ID( function 파일 참조 )
	 * CPPWD	: 다날에서 제공해 드린 PWD( function 파일 참조 )
	 ******************************************************/
	$TransR["CPID"] = $ID;
	$TransR["CPPWD"] = $PWD;

	/***[ 선택 사항 ]**************************************/
	/******************************************************
	 * USERID	: 사용자 ID
	 * ORDERID	: CP 주문번호 
	 * TARGETURL	: 인증 완료 시 이동 할 페이지의 FULL URL
	 * AGELIMIT	: 서비스 사용 제한 나이 설정( 가맹점 필요 시 사용 )
	 ******************************************************/
	$TransR["USERID"] = "USERID"; 
	$TransR["ORDERID"] = $ORDERID;
	$TransR["TARGETURL"] = "http://52.78.144.116/Danal/UAS/Web/CPCGI.php";
	$TransR["AGELIMIT"] = "019";

	/******************************************************************************** 
	 *
	 * [ CPCGI에 HTTP POST로 전달되는 데이터 ] **************************************
	 *
	 ********************************************************************************/

	/***[ 필수 데이터 ]************************************/
	$ByPassValue = array();

	/******************************************************
	 * BgColor	: 인증 페이지 Background Color 설정
	 * BackURL	: 에러 발생 및 취소 시 이동 할 페이지의 FULL URL
	 * IsCharSet	: charset 지정( EUC-KR:deault, UTF-8 )
	 ******************************************************/
	$ByPassValue["BgColor"] = GetRandom( 0,10 ); 
	$ByPassValue["BackURL"] = "http://52.78.144.116/Danal/UAS/Web/BackURL.php";
	$ByPassValue["IsCharSet"] = $CHARSET;

	/***[ 선택 사항 ]**************************************/
	/******************************************************
	 ** CPCGI에 POST DATA로 전달 됩니다.
	 **
	 ******************************************************/
	$ByPassValue["ByBuffer"] = "This value bypass to CPCGI Page";
	$ByPassValue["ByAnyName"] = "AnyValue";

	$Res = CallTrans( $TransR,false );

	if( $Res["RETURNCODE"] == "0000" ) {
?>
<body>
<form name="Ready" action="https://wauth.teledit.com/Danal/WebAuth/Web/Start.php" method="post"> 
<?php
	MakeFormInput($Res,array("RETURNCODE","RETURNMSG"));
	MakeFormInput($ByPassValue);
?>
</form>
<script Language="JavaScript">
	document.Ready.submit();
</script>
</body>
</html>
<?php
	} else {
		/**************************************************************************
		 *
		 * 실패에 대한 작업
		 *
		 **************************************************************************/
		$Result		= $Res["RETURNCODE"];
		$ErrMsg		= $Res["RETURNMSG"];
		$AbleBack	= false;
		$BackURL	= $ByPassValue["BackURL"];
		$BgColor 	= $ByPassValue["BgColor"];
		
		include( "./Error.php" );
	}
?>
