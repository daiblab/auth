<?php
	header( "Pragma: No-Cache" );
	include( "./inc/function.php" );

	/********************************************************************************
	 *
	 * 다날 본인인증
	 *
	 * - 인증 확인 페이지
	 *      인증 확인 및 기타 정보 수신
	 *
	 * 문의사항이 있으시면 아래의 메일주소로 문의 주시기 바랍니다.
	 * DANAL Commerce Division Technique supporting Team
	 * EMAIL : tech@danal.co.kr
	 *
	 ********************************************************************************/

	/********************************************************************************
	 *
	 * XSS 취약점 방지를 위해 
	 * 모든 페이지에서 파라미터 값에 대해 검증하는 로직을 추가할 것을 권고 드립니다.
	 * XSS 취약점이 존재할 경우 웹페이지를 열람하는 접속자의 권한으로 부적절한 스크립트가 수행될 수 있습니다.
	 * 보안 대책
	 *  - html tag를 허용하지 않아야 합니다.(html 태그 허용시 white list를 선정하여 해당 태그만 허용)
	 *  - <, >, &, " 등의 문자를 replace등의 문자 변환함수를 사용하여 치환해야 합니다.
	 * 
	 ********************************************************************************/
?>
<html>
<head>
<title>다날 본인인증</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
</head>
<?php
	$TransR = array();

	$Addition = array( "TID" );
	$TransR = MakeAddtionalInput( $TransR,$_POST,$Addition );

	/*
	 * CONFIRM
	 * - CONFIRMOPTION 
	 *	0 : NONE( default )
	 * 	1 : CPID 및 ORDERID 체크 
	 * - IDENOPTION
	 * 0 : 생년월일(6자리) 및 성별 IDEN 필드로 Return (ex : 1401011)
	 * 1 : 생년월일(8자리) 및 성별 별개 필드로 Return (연동 매뉴얼 참조. ex : DOB=20140101&SEX=1)	 
	 */
	$nConfirmOption = 0; 
	$nIdenOption = 0; 
	$TransR["TXTYPE"] = "CONFIRM";
	$TransR["CONFIRMOPTION"] = $nConfirmOption;
	$TransR["IDENOPTION"] = $nIdenOption;

	/*
	 * CONFIRMOPTION이 1이면 CPID 및 ORDERID 필수 전달
	 */
	if( $nConfirmOption )
	{
		$TransR["CPID"] = $ID;
		$TransR["ORDERID"] = $ORDERID;
	}

	$Res = CallTrans( $TransR,false );

	if( $Res["RETURNCODE"] == "0000" )
	{
		/**************************************************************************
		 *
		 * 인증 성공에 대한 작업 
		 *
		 **************************************************************************/
?>
<body>
<form name="Success" action="./Success.php" method="post">
<?php
	MakeFormInput($_POST,array("TID"));
	MakeFormInput($Res,array("RETURNCODE","RETURNMSG"));
?>
</form>
<script>
	document.Success.submit();
</script>
</body>
</html>
<?php
	} else {
		/**************************************************************************
		 *
		 * 인증 실패에 대한 작업 
		 *
		 **************************************************************************/
		$RETURNCODE		= $Res["RETURNCODE"];
		$RETURNMSG		= $Res["RETURNMSG"];
		$AbleBack	= false;
		$BackURL	= $_POST["BackURL"];
		$BgColor	= $_POST["BgColor"];

		include( "./Error.php" );
	}
?>
